from http.client import BAD_REQUEST, UNAUTHORIZED, FORBIDDEN
from typing import Dict, List

from fastapi import Request, Response, HTTPException
from fastapi.responses import JSONResponse
from pursuitlib.utils import is_null_or_white_space
from pymongo import MongoClient
from pymongo.database import Database

from settings import DB_NAME, ADMIN_TOKEN

JSON_MIME_TYPE = "application/json"
AUTH_HEADER = "Authorization"
TOKEN_AUTH_SCHEME = "Bearer"
PAYLOAD_FIELD = "__Payload"
MESSAGE_FIELD = "Message"


# Returns a valid JSON root element associated with this data
def get_json_payload(data):
    if isinstance(data, Dict) or isinstance(data, List):
        return data
    else: return {PAYLOAD_FIELD: data}


def render_json(data, status_code: int = 200) -> Response:
    return JSONResponse(get_json_payload(data), status_code=status_code)


def render_json_message(message: str, status: int = 200) -> Response:
    return render_json({MESSAGE_FIELD: message}, status)


def render_success_message() -> Response:
    return render_json_message("Operation completed successfully")


def get_database(request: Request) -> Database:
    # The backend is initialized in the "handle_database_session" middleware
    backend: MongoClient = request.state.backend
    return backend.get_database(DB_NAME)


def get_auth_token(request: Request) -> str:
    authorization = request.headers.get(AUTH_HEADER, "")
    if authorization.startswith(f"{TOKEN_AUTH_SCHEME} "):
        return authorization[len(TOKEN_AUTH_SCHEME)+1:]
    return ""


async def get_request_data(request: Request):
    content_type = request.headers.get("Content-Type", "")

    if content_type == JSON_MIME_TYPE or content_type.startswith(f"{JSON_MIME_TYPE};"):
        data = await request.json()
        if PAYLOAD_FIELD in data:  # If the data contains a payload (typically, a JSON value), unpack it
            return data[PAYLOAD_FIELD]
        return data
    else: raise HTTPException(BAD_REQUEST, f"Expected JSON input, but Content-Type is not equal to '{JSON_MIME_TYPE}'")


def ensure_is_admin(request: Request):
    auth_token = get_auth_token(request)
    if is_null_or_white_space(auth_token):
        raise HTTPException(UNAUTHORIZED)
    if auth_token != ADMIN_TOKEN:
        raise HTTPException(FORBIDDEN)

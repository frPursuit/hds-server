from http.client import NOT_FOUND, BAD_REQUEST
from typing import Optional, List, Dict

from fastapi import HTTPException
from pursuitlib.utils import is_null_or_empty
from pymongo.collection import Collection
from pymongo.database import Database
from pymongo.errors import OperationFailure

from utils import PAYLOAD_FIELD

# Constants

STORE_SCOPE = "Scope"
STORE_DATA = "Data"

GLOBAL_SCOPE = "Global"
USER_SCOPE = "User"


# Utility functions

# Fetch data from the MongoDB collection
# Returns None if no data was found
def fetch(store: Collection, pipeline) -> Optional:
    try:
        result = store.aggregate(pipeline).next()
        if PAYLOAD_FIELD in result:
            return result[PAYLOAD_FIELD]
        else: raise HTTPException(NOT_FOUND)
    except (StopIteration, OperationFailure):
        raise HTTPException(NOT_FOUND)


def get_path_elements(path: str) -> List[str]:
    return list(filter(lambda e: not is_null_or_empty(e), path.split('/')))


def is_array_element(path_element: str) -> bool:
    return path_element.startswith("$")


def get_array_index(path_element):
    return int(path_element[1:])


def path_to_pipeline(path: str) -> List:
    pipeline = []

    partial_path = [STORE_DATA]
    for elem in get_path_elements(path):
        if is_array_element(elem):
            array_index = get_array_index(elem)
            pipeline.append({
                "$project": {
                    PAYLOAD_FIELD: {
                        "$arrayElemAt": [f"${'.'.join(partial_path)}", array_index]
                    }
                }
            })
            partial_path = [PAYLOAD_FIELD]
        else: partial_path.append(elem)

    pipeline.append({
        "$project": {
            PAYLOAD_FIELD: f"${'.'.join(partial_path)}"
        }
    })

    return pipeline


def update(store: Collection, filter_obj, update_obj):
    try:
        store.update_one(filter_obj, update_obj)
    except OperationFailure as e:
        raise HTTPException(BAD_REQUEST, str(e))


def path_to_dot(path: str) -> str:
    dot = STORE_DATA

    for elem in get_path_elements(path):
        if is_array_element(elem):
            array_index = get_array_index(elem)
            dot += f".{array_index}"
        else: dot += f".{elem}"

    return dot


# Generic API

def get_store_names(db: Database) -> List[str]:
    return db.list_collection_names()


def does_store_exist(db: Database, name: str) -> bool:
    return name in get_store_names(db)


def get_store(db: Database, name: str) -> Collection:
    if does_store_exist(db, name):
        return db.get_collection(name)
    else: raise HTTPException(NOT_FOUND)


def delete_store(db: Database, name: str):
    if does_store_exist(db, name):
        db.drop_collection(name)
    else: raise HTTPException(NOT_FOUND)


def get_from_store(store: Collection, filter_obj, path: str):
    pipeline = [
        {
            "$match": filter_obj
        }
    ] + path_to_pipeline(path)
    return fetch(store, pipeline)


# Remove "null" elements from an array
def clean_store_array(store: Collection, filter_obj, path: str):
    dot = path_to_dot(path)
    update(store, filter_obj, {"$pull": {dot: None}})


def put_to_store(store: Collection, filter_obj, path: str, data):
    dot = path_to_dot(path)
    path_elements = get_path_elements(path)
    last_element = path_elements[len(path_elements) - 1] if len(path_elements) > 0 else None
    is_array = is_array_element(last_element) if last_element is not None else None

    update(store, filter_obj, {"$set": {dot: data}})

    # If an element is put into an array, ensure no "null" elements are added in between
    if is_array:
        clean_store_array(store, filter_obj, '/'.join(path_elements[:-1]))


def post_to_store(store: Collection, filter_obj, path: str, data):
    try:
        # This will fail if the specified element already exists
        get_from_store(store, filter_obj, path)
        raise HTTPException(BAD_REQUEST, "The specified element already exists")
    except HTTPException as e:
        if e.status_code == NOT_FOUND:
            put_to_store(store, filter_obj, path, data)
        else: raise e


def patch_store(store: Collection, filter_obj, path: str, data):
    if not isinstance(data, Dict):
        raise HTTPException(BAD_REQUEST, "Only JSON objects can be used as a patch source")

    buffer = get_from_store(store, filter_obj, path)
    if not isinstance(buffer, Dict):
        raise HTTPException(BAD_REQUEST, "Only JSON objects can be patched")

    for key, value in data.items():
        buffer[key] = value
    put_to_store(store, filter_obj, path, buffer)


def delete_from_store(store: Collection, filter_obj, path: str):
    dot = path_to_dot(path)
    path_elements = get_path_elements(path)
    last_element = path_elements[len(path_elements) - 1] if len(path_elements) > 0 else None
    is_array = is_array_element(last_element) if last_element is not None else None

    update(store, filter_obj, {"$unset": {dot: ""}})

    # If an array element was removed, it has been replaced with a "null" element that needs to be removed
    if is_array:
        clean_store_array(store, filter_obj, '/'.join(path_elements[:-1]))


# Global view API

def get_global_filter():
    return {STORE_SCOPE: GLOBAL_SCOPE}


def create_global_store(db: Database, name: str, data):
    if not does_store_exist(db, name):
        store = db.create_collection(name)
        document = get_global_filter()
        document[STORE_DATA] = data
        store.insert_one(document)
    else: raise HTTPException(BAD_REQUEST, "The specified data store already exists")


def get_global(store: Collection, path: str):
    return get_from_store(store, get_global_filter(), path)


def put_global(store: Collection, path: str, data):
    put_to_store(store, get_global_filter(), path, data)


def post_global(store: Collection, path: str, data):
    post_to_store(store, get_global_filter(), path, data)


def patch_global(store: Collection, path: str, data):
    patch_store(store, get_global_filter(), path, data)


def delete_global(store: Collection, path: str):
    delete_from_store(store, get_global_filter(), path)


# DataStore creation

def create_store(db: Database, name: str, info):
    if STORE_SCOPE not in info:
        raise HTTPException(BAD_REQUEST, f"Missing '{STORE_SCOPE}' attribute")
    scope = info[STORE_SCOPE]
    data = info[STORE_DATA] if STORE_DATA in info else {}

    if scope == GLOBAL_SCOPE:
        create_global_store(db, name, data)
    elif scope == USER_SCOPE:
        # In the future, this will only create an empty collection
        # Documents will be added by users on the fly
        raise HTTPException(BAD_REQUEST, "User Data Stores are not yet implemented")
    else: raise HTTPException(BAD_REQUEST, "Invalid Data Store scope")

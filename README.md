# HDS (Hierarchical Data Store) server

Each DataStore corresponds to a MongoDB collection

There is two DataStore types:

- Global DataStores
- User-specific DataStores

Collections that correspond to Global DataStores only contain one document,
with a "Global" scope

Collections that correspond to User DataStores contain one document per user.

## Configuring the environment

To configure environment variables, create a `.env` file based on `.env.example`.

## Docker compose

Docker compose can be used in testing to start a local MongoDB database.

To start the database, use the `docker-compose up -d` command.

To stop the database, use `docker-compose down`.

To delete the database data, use `docker-compose down -v`

import traceback
from http.client import METHOD_NOT_ALLOWED, INTERNAL_SERVER_ERROR, BAD_REQUEST

from fastapi import FastAPI, Request, Response, HTTPException
from pursuitlib.utils import is_null_or_empty
from pymongo import MongoClient
from pymongo.collection import Collection
from starlette.exceptions import HTTPException as StarletteHTTPException

import hds
from settings import APP_NAME, DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, VERSION, DISPLAY_VERSION, DB_NAME, DB_USER_GLOBAL
from utils import render_json_message, get_database, render_json, render_success_message, get_request_data, \
    ensure_is_admin

app = FastAPI(title=APP_NAME, openapi_url=None, docs_url=None, redoc_url=None)


async def datastore_element(request: Request, store: Collection, path: str) -> Response:
    # Due to a MongoDB limitation, '.' is a reserved character
    if '.' in path:
        raise HTTPException(BAD_REQUEST, "The requested path contains '.', which is an illegal character")

    if request.method == "GET":
        obj = hds.get_global(store, path)
        return render_json(obj)
    elif request.method == "POST":
        ensure_is_admin(request)
        hds.post_global(store, path, await get_request_data(request))
        return render_success_message()
    elif request.method == "PUT":
        ensure_is_admin(request)
        hds.put_global(store, path, await get_request_data(request))
        return render_success_message()
    elif request.method == "PATCH":
        ensure_is_admin(request)
        hds.patch_global(store, path, await get_request_data(request))
        return render_success_message()
    elif request.method == "DELETE":
        ensure_is_admin(request)
        hds.delete_global(store, path)
        return render_success_message()
    else: raise HTTPException(METHOD_NOT_ALLOWED)


# Server metadata

@app.get("/")
async def get_server_metadata(request: Request) -> Response:
    db = get_database(request)

    ensure_is_admin(request)

    # Each datastore corresponds to a MongoDB collection
    datastores = db.list_collection_names()

    return render_json({
        "DataStores": datastores,
        "Version": VERSION,
        "DisplayVersion": DISPLAY_VERSION
    })


@app.api_route("/{sname:str}/", methods=["GET", "POST", "PUT", "PATCH", "DELETE"])
async def datastore_root(request: Request, sname: str) -> Response:
    db = get_database(request)

    if request.method == "POST":
        ensure_is_admin(request)
        hds.create_store(db, sname, await get_request_data(request))
        return render_success_message()
    elif request.method == "DELETE":
        ensure_is_admin(request)
        hds.delete_store(db, sname)
        return render_success_message()
    else:
        store = hds.get_store(db, sname)
        return await datastore_element(request, store, "")


@app.api_route("/{sname:str}/{path:path}", methods=["GET", "POST", "PUT", "PATCH", "DELETE"])
async def datastore_path(request: Request, sname: str, path: str) -> Response:
    db = get_database(request)
    store = hds.get_store(db, sname)
    return await datastore_element(request, store, path)


# Exception handling

@app.exception_handler(StarletteHTTPException)
async def handle_exception(request: Request, exception: StarletteHTTPException) -> Response:
    return render_json_message(f"Error {exception.status_code}: {exception.detail}", exception.status_code)


# Middleware

@app.middleware("http")
async def catch_exception(request: Request, call_next) -> Response:
    try:
        return await call_next(request)
    except Exception as e:
        traceback.print_exception(e)
        if isinstance(e, HTTPException):
            return await handle_exception(request, e)
        else: return await handle_exception(request, HTTPException(INTERNAL_SERVER_ERROR, str(e)))


@app.middleware("http")
async def handle_database_session(request: Request, call_next) -> Response:
    try:
        host = f"{DB_HOST}:{DB_PORT}" if not is_null_or_empty(DB_PORT) else DB_HOST
        if DB_USER_GLOBAL:
            connection = f"mongodb://{DB_USER}:{DB_PASSWORD}@{host}/"
        else: connection = f"mongodb://{DB_USER}:{DB_PASSWORD}@{host}/{DB_NAME}"

        request.state.backend = MongoClient(connection)
        response = await call_next(request)
        request.state.backend.close()
        return response
    except Exception as e:
        return await handle_exception(request, HTTPException(INTERNAL_SERVER_ERROR, f"Unable to connect to the backend: {e}"))

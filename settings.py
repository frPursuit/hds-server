from pursuitlib.utils import get_env


# Application settings

APP_NAME = "hds-server"

VERSION = "0.2.0"
DEV_STAGE = "BETA"
DISPLAY_VERSION = VERSION if DEV_STAGE is None else VERSION + " " + DEV_STAGE


# MongoDB backend

DB_NAME = get_env("DB_NAME")
DB_USER = get_env("DB_USER")
DB_PASSWORD = get_env("DB_PASSWORD")
DB_HOST = get_env("DB_HOST")
DB_PORT = get_env("DB_PORT", "")
DB_USER_GLOBAL = get_env("DB_USER_GLOBAL", "False").lower() == "true"


# Administration token

ADMIN_TOKEN = get_env("ADMIN_TOKEN")

#!/bin/bash

# Exit when any command fails
set -e

# Start the production server
uvicorn main:app --host 0.0.0.0 --port $APP_PORT
